exibir_menu() {
    echo "---------------------------"
    echo "Jogo do Dragão - Menu"
    echo "1. Atacar"
    echo "2. Fugir"
    echo "3. Info"
    echo "---------------------------"
}

calcular_dano() {
    local min=$1
    local max=$2
    echo $((RANDOM % (max - min + 1) + min))
}

calcular_cura() {
    local min=$1
    local max=$2
    echo $((RANDOM % (max - min + 1) + min))
}

exibir_vida() {
    echo "Sua vida: $1"
    echo "Vida do Apófis: $2"
}

exibir_fala_do_dragao() {
    local mensagem=$1
    cowsay -f dragon "$mensagem"
}
